# Step-project-ham 
# Project`s title : HAM.
This project is a Step-Project for DAN-IT course of Frontend Developer. Simple web page created with HTML, CSS and JavaScript.
# Description
Simple project has an interactive and user friendly interface. 
Use the menu links to jump between sections.



  There are interactive menu about services (when you click on the button, the text of the description changes), hover-sensitive cards and fitration. Also, there is a button "Load more" which simulates loading content and adds + 12 more photos when you click on it. It is possible to press the button twice, and add another + 12 photos and only then the button disappears. Another section has a carousel that can be switched with the mouse (arrows on the carousel itself) and with the keyboard arrows - at the same time, the avatar and text of the review change.

Load more, filtration, carousel is made with vanilla JS.
CSS separated into separate files.

# Executing program

Deployment: https://ingridnovak.github.io/old-ingridnovak.github.io/

